from kivy.core.window import Window
from kivy.uix.actionbar import ActionGroup


class HLGroup(ActionGroup):

    def __init__(self, *args, **kwargs):
        super(HLGroup, self).__init__(*args, **kwargs)
        Window.bind(mouse_pos=self.pos_check)

    def pos_check(self, inst, pos):
        if self.collide_point(*pos):
             self.background_color = (0, 0, 0, 1)
        else:
             self.background_color = (1,1,1,1)